# DjangoGirls Tutorial

This is a walk-through of the [DjangoGirls
tutorial](https://tutorial.djangogirls.org/en/) started 2020.02.01 in
preparation for the Arlington workshop 2020.02.22.

Aside from `pipenv` in place of `virtualenv` and `pip`, I don't expect
to go fare "off book".

Actually, in addition, I'm using [Codeberg](https://codeberg.org/) in
place of GitHub, and skipping ahead to the actual Django part, boldly
assuming I know Bash and Python 3 well enough. ;-)

The [PythonAnywhere](https://www.pythonanywhere.com) username is, of
course, `ubuntourist`.

## Beginning in earnest

Create a PythonAnywhere API key. Python Anywhere helpfully offers
sample usage code with the newly created key pasted in:

    import requests
    username = "ubuntourist"
    token = "2181f5aeec5b71d2775931338d9daf2d556003fd"

    response = requests.get("https://www.pythonanywhere.com/api/v0/user/{username}/cpu/"
                            .format(username=username),
                            headers={"Authorization": "Token {token}"
                                    .format(token=token)})
    if response.status_code == 200:
        print("CPU quota info:")
        print(response.content)
    else:
        print("Got unexpected status code {}: {!r}"
              .format(response.status_code, response.content))

Set up the virtual environment, install Django into it, and initialize
a new Django project:

    $ pipenv shell
    $ emacs Pipfile
    ...
    [packages]
    Django = "==2.2.4"
    ...
    $ pipenv update
    $ django-admin startproject mysite .

    $ ls -l mysite/
    total 12
    -rw-r--r-- 1 kjcole kjcole    0 Feb  1 11:40 __init__.py
    -rw-r--r-- 1 kjcole kjcole 3263 Feb  1 12:18 settings.py
    -rw-r--r-- 1 kjcole kjcole  796 Feb  1 12:12 urls.py
    -rw-r--r-- 1 kjcole kjcole  437 Feb  1 12:12 wsgi.py

Create a database. In settings this is named "sb.sqlite3" (the default
created by the `django-admin startproject` command). Not the most
imaginative of names, but I'll live with it.

    $ ./manage.py migrate
    Operations to perform:
      Apply all migrations: admin, auth, contenttypes, sessions
    Running migrations:
      Applying contenttypes.0001_initial... OK
      Applying auth.0001_initial... OK
      Applying admin.0001_initial... OK
      Applying admin.0002_logentry_remove_auto_add... OK
      Applying admin.0003_logentry_add_action_flag_choices... OK
      Applying contenttypes.0002_remove_content_type_name... OK
      Applying auth.0002_alter_permission_name_max_length... OK
      Applying auth.0003_alter_user_email_max_length... OK
      Applying auth.0004_alter_user_username_opts... OK
      Applying auth.0005_alter_user_last_login_null... OK
      Applying auth.0006_require_contenttypes_0002... OK
      Applying auth.0007_alter_validators_add_error_messages... OK
      Applying auth.0008_alter_user_username_max_length... OK
      Applying auth.0009_alter_user_last_name_max_length... OK
      Applying auth.0010_alter_group_name_max_length... OK
      Applying auth.0011_update_proxy_permissions... OK
      Applying sessions.0001_initial... OK
    $

Create an app within the project:

    $ ./manage.py startapp blog
    $ ls -lA blog/
    total 24
    -rw-r--r-- 1 kjcole kjcole   63 Feb  1 13:06 admin.py
    -rw-r--r-- 1 kjcole kjcole   83 Feb  1 13:06 apps.py
    -rw-r--r-- 1 kjcole kjcole    0 Feb  1 13:06 __init__.py
    drwxr-xr-x 2 kjcole kjcole 4096 Feb  1 13:06 migrations
    -rw-r--r-- 1 kjcole kjcole   57 Feb  1 13:06 models.py
    -rw-r--r-- 1 kjcole kjcole   60 Feb  1 13:06 tests.py
    -rw-r--r-- 1 kjcole kjcole   63 Feb  1 13:06 views.py

Create empty blog tables:

    $ ./manage.py makemigrations blog
    Migrations for 'blog':
      blog/migrations/0001_initial.py
        - Create model Post
    kjcole@ncc-1701:~/gits/djangogirls$ ./manage.py migrate blog
    Operations to perform:
      Apply all migrations: blog
    Running migrations:
      Applying blog.0001_initial... OK
    $

This added a `migrations` subdirectory to the `blog` subdirectory with
two files (aside from the ubiquitous `pycache`):

    blog/migrations:
    total 8
    -rw-r--r-- 1 kjcole kjcole  989 Feb  6 13:56 0001_initial.py
    -rw-r--r-- 1 kjcole kjcole    0 Feb  1 13:06 __init__.py

(NOTE: for the `manage.py` migration commands, I forgot to use the
virtual environment and ended up using the system-wide Django, which
is almost assuredly more recent than the virtual environment version.)

Add a superuser:

    $ ./manage.py createsuperuser
    Username (leave blank to use 'kjcole'): ubuntourist
    Email address: kevin.cole@novawebdevelopment.org
    Password:
    Password (again):
    Superuser created successfully.
    $

## DEPLOY!

So, crank up a `Bash` console on PythonAnywhere, and feed it:

    pip3.6 install --user pythonanywhere

There were already a honking **462** Python packages installed! This
makes 463.

    $ pa_autoconfigure_django.py --python=3.6 \
      https://codeberg.org/ubuntourist/djangogirls_tutorial.git

And... alas. No. Too clever for my own good.

    Received HTTP code 403 from proxy after CONNECT

According to the [PythonAnywhere
forum](https://www.pythonanywhere.com/forums/topic/1595/) sites need
to be whitelisted and according to one responder:

> The requirements for whitelisting a site are that it should have a
> public API and be useful for other free users. ...just post a link
> to their API docs and I'll add it to the whitelist.

So, now I have an
[issue](https://codeberg.org/Codeberg/Community/issues/131) filed for
Codeberg's repository, asking if they have the requisite __public
API__...

And... a mere four days later... Everyone's happy. Issue resolved
by both Codeberg.org and PythonAnywhere.com quickly. Yay!

    $ pa_autoconfigure_django.py --python=3.6 https://codeberg.org/ubuntourist/djangogirls_tutorial.git
    < Running API sanity checks >
       \
        ~<:>>>>>>>>>
    Cloning into '/home/ubuntourist/ubuntourist.pythonanywhere.com'...
    remote: Enumerating objects: 111, done.
    remote: Counting objects: 100% (111/111), done.
    remote: Compressing objects: 100% (109/109), done.
    remote: Total 111 (delta 57), reused 0 (delta 0)
    Receiving objects: 100% (111/111), 31.38 KiB | 0 bytes/s, done.
    Resolving deltas: 100% (57/57), done.
    Checking connectivity... done.
    < Creating virtualenv with Python3.6 >
       \
        ~<:>>>>>>>>>
    Running virtualenv with interpreter /usr/bin/python3.6
    Already using interpreter /usr/bin/python3.6
    Using base prefix '/usr'
    New python executable in /home/ubuntourist/.virtualenvs/ubuntourist.pythonanywhere.com/bin/python3.6
    Also creating executable in /home/ubuntourist/.virtualenvs/ubuntourist.pythonanywhere.com/bin/python
    Installing setuptools, pip, wheel...
    done.
    virtualenvwrapper.user_scripts creating /home/ubuntourist/.virtualenvs/ubuntourist.pythonanywhere.com/bin/predeactivate
    virtualenvwrapper.user_scripts creating /home/ubuntourist/.virtualenvs/ubuntourist.pythonanywhere.com/bin/postdeactivate
    virtualenvwrapper.user_scripts creating /home/ubuntourist/.virtualenvs/ubuntourist.pythonanywhere.com/bin/preactivate
    virtualenvwrapper.user_scripts creating /home/ubuntourist/.virtualenvs/ubuntourist.pythonanywhere.com/bin/postactivate
    virtualenvwrapper.user_scripts creating /home/ubuntourist/.virtualenvs/ubuntourist.pythonanywhere.com/bin/get_env_details

    < Pip installing django (this may take a couple of minutes) >
       \
        ~<:>>>>>>>>>
    Looking in links: /usr/share/pip-wheels
    Collecting django
      Downloading Django-3.0.3-py3-none-any.whl (7.5 MB)
         |████████████████████████████████| 7.5 MB 19.4 MB/s
    Processing /usr/share/pip-wheels/pytz-2019.3-py2.py3-none-any.whl
    Processing /usr/share/pip-wheels/sqlparse-0.3.0-py2.py3-none-any.whl
    Collecting asgiref~=3.2
      Downloading asgiref-3.2.3-py2.py3-none-any.whl (18 kB)
    Installing collected packages: pytz, sqlparse, asgiref, django
    Successfully installed asgiref-3.2.3 django-3.0.3 pytz-2019.3 sqlparse-0.3.0
    < Creating web app via API >
       \
        ~<:>>>>>>>>>
    < Adding static files mappings for /static/ and /media/ >
       \
        ~<:>>>>>>>>>
    < Updating wsgi file at /var/www/ubuntourist_pythonanywhere_com_wsgi.py >
       \
        ~<:>>>>>>>>>
    < Updating settings.py >
       \
        ~<:>>>>>>>>>
    < Running collectstatic >
       \
        ~<:>>>>>>>>>
    130 static files copied to '/home/ubuntourist/ubuntourist.pythonanywhere.com/static'.
    < Running migrate database >
       \
        ~<:>>>>>>>>>
    Operations to perform:
      Apply all migrations: admin, auth, blog, contenttypes, sessions
    Running migrations:
      Applying contenttypes.0001_initial... OK
      Applying auth.0001_initial... OK
      Applying admin.0001_initial... OK
      Applying admin.0002_logentry_remove_auto_add... OK
      Applying admin.0003_logentry_add_action_flag_choices... OK
      Applying contenttypes.0002_remove_content_type_name... OK
      Applying auth.0002_alter_permission_name_max_length... OK
      Applying auth.0003_alter_user_email_max_length... OK
      Applying auth.0004_alter_user_username_opts... OK
      Applying auth.0005_alter_user_last_login_null... OK
      Applying auth.0006_require_contenttypes_0002... OK
      Applying auth.0007_alter_validators_add_error_messages... OK
      Applying auth.0008_alter_user_username_max_length... OK
      Applying auth.0009_alter_user_last_name_max_length... OK
      Applying auth.0010_alter_group_name_max_length... OK
      Applying auth.0011_update_proxy_permissions... OK
      Applying blog.0001_initial... OK
      Applying sessions.0001_initial... OK
    < Reloading ubuntourist.pythonanywhere.com via API >
       \
        ~<:>>>>>>>>>
      ______________________________________
    /                                        \
    | All done!  Your site is now live at    |
    | https://ubuntourist.pythonanywhere.com |
    \                                        /
      --------------------------------------
       \
        ~<:>>>>>>>>>
      ___________________________________________________________________
    /                                                                     \
    | Starting Bash shell with activated virtualenv in project directory. |
    | Press Ctrl+D to exit.                                               |
    \                                                                     /
      -------------------------------------------------------------------
       \
        ~<:>>>>>>>>>
    (ubuntourist.pythonanywhere.com) 12:31 ~/ubuntourist.pythonanywhere.com (master)$

But the database needs to be freshly created, and a superuser as well.

    $ ./manage.py createsuperuser
    Username (leave blank to use 'ubuntourist'):
    Email address: kevin.cole@novawebdevelopment.org
    Password:
    Password (again):
    Superuser created successfully.

Browsing https://ubuntourist.pythonanywhere.com/ yields a Django Dump:

    DisallowedHost at /

    Invalid HTTP_HOST header: 'ubuntourist.pythonanywhere.com'.
	You may need to add 'ubuntourist.pythonanywhere.com' to ALLOWED_HOSTS.

Sounds like a plan / good suggestion to me....

## BAD DOG!!!

Who's a bad dog? PythonAnywhere (or possibly DjangoGirls)!  So.  In my
**DEVELOPMENT** repository, I dutifully changed the `ALLOWED_HOSTS` in
`settings.py`.  However, unbeknownst to me, that
`pa_autoconfigure_django.py` command that spewed out a bunch of stuff
went messing with the **PRODUCTION** repository, also changing
`settings.py`.

    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)
            modified:   mysite/settings.py
    no changes added to commit (use "git add" and/or "git commit -a")
    12:48 ~/ubuntourist.pythonanywhere.com (master)$ git diff
    diff --git a/mysite/settings.py b/mysite/settings.py
    index 311307b..8935907 100644
    --- a/mysite/settings.py
    +++ b/mysite/settings.py
    @@ -124,3 +124,7 @@ USE_TZ = True

     STATIC_URL = "/static/"
     STATIC_ROOT = os.path.join(BASE_DIR, "static")
    +
    +MEDIA_URL = '/media/'
    +STATIC_ROOT = os.path.join(BASE_DIR, 'static')
    +MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

The simplest solution (to me) is to revert the changes on
PythonAnywhere, add the changes locally, push from local, and pull
from PythonAnywhere.  Here goes...

Well. That worked but the old server remains running somewhere:

    $ ./manage.py runserver
    Watching for file changes with StatReloader
    Performing system checks...
    System check identified no issues (0 silenced).
    February 08, 2020 - 09:01:14
    Django version 3.0.3, using settings 'mysite.settings'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.
    Error: That port is already in use.

To restart: Go to the
[PythonAnywhere](https://www.pythonanywhere.com/) Dashboard.
In the upper right, click **Web**. On the next page, click the
green `[Reload <username>.pythonanywhere.com]` button.


**NOTE** We need to know where PythonAnywhere hides its virtual
environment...

    $ pwd
    /home/ubuntourist
    $ ls -AlF
    total 48
    -rw------- 1 ubuntourist registered_users   64 Feb  8 12:47 .bash_history
    -rwxr-xr-x 1 ubuntourist registered_users  559 Feb  1 16:31 .bashrc*
    drwxrwxr-x 3 ubuntourist registered_users 4096 Feb  6 20:46 .cache/
    -rwxr-xr-x 1 ubuntourist registered_users  266 Feb  1 16:31 .gitconfig*
    drwxrwxr-x 4 ubuntourist registered_users 4096 Feb  6 20:47 .local/
    -rw-r--r-- 1 ubuntourist registered_users   79 Feb  1 16:31 .profile
    -rwxr-xr-x 1 ubuntourist registered_users   77 Feb  1 16:31 .pythonstartup.py*
    -rwxr-xr-x 1 ubuntourist registered_users 4693 Feb  1 16:31 .vimrc*
    drwxrwxr-x 3 ubuntourist registered_users 4096 Feb  8 12:26 .virtualenvs/
    -rwxr-xr-x 1 ubuntourist registered_users  232 Feb  1 16:31 README.txt*
    drwxrwxr-x 6 ubuntourist registered_users 4096 Feb  8 12:59 ubuntourist.pythonanywhere.com/

    $ ls -AlF .virtualenvs/
    total 52
    -rwxr-xr-x 1 ubuntourist registered_users  135 Feb  6 20:43 get_env_details*
    -rw-r--r-- 1 ubuntourist registered_users   96 Feb  6 20:43 initialize
    -rw-r--r-- 1 ubuntourist registered_users   73 Feb  6 20:43 postactivate
    -rw-r--r-- 1 ubuntourist registered_users   75 Feb  6 20:43 postdeactivate
    -rwxr-xr-x 1 ubuntourist registered_users   66 Feb  6 20:43 postmkproject*
    -rw-r--r-- 1 ubuntourist registered_users   73 Feb  6 20:43 postmkvirtualenv
    -rwxr-xr-x 1 ubuntourist registered_users  110 Feb  6 20:43 postrmvirtualenv*
    -rwxr-xr-x 1 ubuntourist registered_users   99 Feb  6 20:43 preactivate*
    -rw-r--r-- 1 ubuntourist registered_users   76 Feb  6 20:43 predeactivate
    -rwxr-xr-x 1 ubuntourist registered_users   91 Feb  6 20:43 premkproject*
    -rwxr-xr-x 1 ubuntourist registered_users  130 Feb  6 20:43 premkvirtualenv*
    -rwxr-xr-x 1 ubuntourist registered_users  111 Feb  6 20:43 prermvirtualenv*
    drwxrwxr-x 5 ubuntourist registered_users 4096 Feb  8 12:26 ubuntourist.pythonanywhere.com/

    $ ls -AlF .virtualenvs/ubuntourist.pythonanywhere.com/bin/
    total 108
    drwxrwxr-x 2 ubuntourist registered_users  4096 Feb  8 12:30 __pycache__/
    -rw-rw-r-- 1 ubuntourist registered_users  2250 Feb  8 12:26 activate
    -rw-rw-r-- 1 ubuntourist registered_users  1469 Feb  8 12:26 activate.csh
    -rw-rw-r-- 1 ubuntourist registered_users  3134 Feb  8 12:26 activate.fish
    -rw-rw-r-- 1 ubuntourist registered_users  1751 Feb  8 12:26 activate.ps1
    -rw-rw-r-- 1 ubuntourist registered_users  1191 Feb  8 12:26 activate.xsh
    -rw-rw-r-- 1 ubuntourist registered_users  1517 Feb  8 12:26 activate_this.py
    -rwxrwxr-x 1 ubuntourist registered_users   322 Feb  8 12:30 django-admin*
    -rwxrwxr-x 1 ubuntourist registered_users   184 Feb  8 12:30 django-admin.py*
    -rwxrwxr-x 1 ubuntourist registered_users   292 Feb  8 12:26 easy_install*
    -rwxrwxr-x 1 ubuntourist registered_users   292 Feb  8 12:26 easy_install-3.6*
    -rwxr-xr-x 1 ubuntourist registered_users   150 Feb  8 12:26 get_env_details*
    -rwxrwxr-x 1 ubuntourist registered_users   283 Feb  8 12:26 pip*
    -rwxrwxr-x 1 ubuntourist registered_users   283 Feb  8 12:26 pip3*
    -rwxrwxr-x 1 ubuntourist registered_users   283 Feb  8 12:26 pip3.6*
    -rw-r--r-- 1 ubuntourist registered_users    72 Feb  8 12:26 postactivate
    -rw-r--r-- 1 ubuntourist registered_users    74 Feb  8 12:26 postdeactivate
    -rwxr-xr-x 1 ubuntourist registered_users    69 Feb  8 12:26 preactivate*
    -rw-r--r-- 1 ubuntourist registered_users    75 Feb  8 12:26 predeactivate
    lrwxrwxrwx 1 ubuntourist registered_users     9 Feb  8 12:26 python -> python3.6*
    -rwxrwxr-x 1 ubuntourist registered_users  2379 Feb  8 12:26 python-config*
    lrwxrwxrwx 1 ubuntourist registered_users     9 Feb  8 12:26 python3 -> python3.6*
    -rwxrwxr-x 1 ubuntourist registered_users 18208 Feb  8 12:26 python3.6*
    -rwxrwxr-x 1 ubuntourist registered_users   275 Feb  8 12:27 sqlformat*
    -rwxrwxr-x 1 ubuntourist registered_users   270 Feb  8 12:26 wheel*

And there you have it, ladies and gentlemen:

    ~/.virtualenvs/ubuntourist.pythonanywhere.com/bin/activate

But:

    $ ~/.virtualenvs/ubuntourist.pythonanywhere.com/bin/activate
    bash: /home/ubuntourist/.virtualenvs/ubuntourist.pythonanywhere.com/bin/activate: Permission denied

Yeuch. A bit of digging shows the correct answer:

    $ workon ubuntourist.pythonanywhere.com

Let's try `pipenv` instead...

First, find out where `pa_autoconfigure_django.py` lives and what it
screws with.

    $ which pa_autoconfigure_django.py
    /home/ubuntourist/.local/bin/pa_autoconfigure_django.py

Too annoying. Just do it.

    $ pip3.6 install --user pipenv
    Looking in links: /usr/share/pip-wheels
    Collecting pipenv
      Using cached https://files.pythonhosted.org/packages/13/b4/3ffa55f77161cff9a5220f162670f7c5eb00df52e00939e203f601b0f579/pipenv-2018.11.26-py3-none-any.whl
    Requirement already satisfied: setuptools>=36.2.1 in /usr/lib/python3.6/site-packages (from pipenv) (41.6.0)
    Requirement already satisfied: certifi in /usr/lib/python3.6/site-packages (from pipenv) (2019.9.11)
    Requirement already satisfied: virtualenv in /usr/lib/python3.6/site-packages (from pipenv) (16.7.7)
    Requirement already satisfied: virtualenv-clone>=0.2.5 in /usr/lib/python3.6/site-packages (from pipenv) (0.5.3)
    Requirement already satisfied: pip>=9.0.1 in /usr/lib/python3.6/site-packages (from pipenv) (19.3.1)
    Installing collected packages: pipenv
    Successfully installed pipenv-2018.11.26

----

For some unknown reason, `href="{% static 'css/blog.css' %}"` kept
rendering as `href="/static/css/blog.css"` and giving me a 404 error.
This was only tested from the development environment, and went away
after a day. (It did not appear to go away after several reloads and
server restarts yesterday...)

Several days later... The deployed site was refusing to deal with the
static stuff as well. For a week. That's because I got ahead of
myself, assuming that I should `git` often. The rule here: Don't
deploy until the tutorial tells you to deploy.  In a later tutorial
page, it infomed me that after doing the `git pull` and reloading I
needed to do:

    workon ubuntourist.pythonanywhere.com
	cd ubuntourist.pythonanywhere.com/
	./manage.py collectstatic

See also PythonAnywhere's [How to setup static files in
Django](https://help.pythonanywhere.com/pages/DjangoStaticFiles)

After that. the remote site began behaving well.

----

