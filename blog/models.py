#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf  import settings
from django.db    import models
from django.utils import timezone


class Post(models.Model):
    """A blog post entry"""
    author           = models.ForeignKey(settings.AUTH_USER_MODEL,
                                         on_delete=models.CASCADE)
    title            = models.CharField(max_length=200)
    body             = models.TextField()
    creation_date    = models.DateTimeField(default=timezone.now)
    publication_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        """Set the publication date of a post to the current time"""
        self.publication_date = timezone.now()
        self.save()

    def __str__(self):
        """Identify each instance of an Post object by title"""
        return self.title

