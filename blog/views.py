#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from .models import Post

def post_list(request):
    """Show all blog posts by publication date"""
    posts = Post.objects
    posts = posts.filter(publication_date__lte=timezone.now())
    posts = posts.order_by("publication_date")
    return render(request, "blog/post_list.html", {"posts": posts})

def post_detail(request, pk):
    """Show a single blog post"""
    post = get_object_or_404(Post, pk=pk)  # primary key from 2nd arg
    return render(request, 'blog/post_detail.html', {'post': post})
